package com.sayil2.sayillessons2.controllers;


import com.sayil2.sayillessons2.exceptions.ResourceNotFound;
import com.sayil2.sayillessons2.models.Employee;
import com.sayil2.sayillessons2.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class EmployeeController {
    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping("/employees")
     public List<Employee> getEmployees(){
     return employeeRepository.findAll();
     }

    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> getEmployee(@PathVariable (value = "id") Long employeeid) throws ResourceNotFound
    {
        Employee employee = employeeRepository.findById(employeeid).orElseThrow(()-> new ResourceNotFound("this user doesnt exist" + employeeid));
        return ResponseEntity.ok().body(employee);

    }

    @PostMapping("/employees")
    public Employee createEmployee(@RequestBody Employee employee){
        return employeeRepository.save(employee);
    }

    @PutMapping("/employee/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable (value = "id") Long employeeid, @Validated @RequestBody Employee employeeDet) throws ResourceNotFound{
        Employee employee = employeeRepository.findById(employeeid).orElseThrow(()-> new ResourceNotFound("this user doesnt exist" + employeeid));
           employee.setEmail(employeeDet.getEmail());
           employee.setFirstname(employeeDet.getFirstname());
           employee.setLastname(employeeDet.getLastname());
           return ResponseEntity.ok(this.employeeRepository.save(employee));
    }

    @DeleteMapping("/employee/{id}")
    public Map<String, Boolean> deleteEmployee(@PathVariable (value = "id") Long employeeid) throws ResourceNotFound{
        Employee employee = employeeRepository.findById(employeeid).orElseThrow(()-> new ResourceNotFound("this user doesnt exist" + employeeid));
        this.employeeRepository.delete(employee);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return  response;
    }
}
