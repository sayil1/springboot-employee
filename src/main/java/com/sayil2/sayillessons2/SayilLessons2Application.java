package com.sayil2.sayillessons2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SayilLessons2Application {

	public static void main(String[] args) {
		SpringApplication.run(SayilLessons2Application.class, args);
	}

}
